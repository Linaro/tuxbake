# -*- coding: utf-8 -*-

"""
Command line tool to build OpenEmbedded and Yocto images
"""
__version__ = "0.8.0"
